// Object: variabel yg memiliki key (name) dan value (nilai)

// Array --> Kekurangannya adalah informasi yang disampaikan belum begitu jelas
let idcard = ['wafa', 17, false, 'jl. jonggrang']; // Array

// Object
let ktp = {
    nama: 'Wafa',
    umur: 17,
    isMarried: false,
    alamat: 'Jalan Jonggrang',
    gpa: [3.00, 3.50, 4.0],
    ratarataGpa: function() {
        let jumlah = 0;
        let totGpa = this.gpa;
        for (let i = 0; i < totGpa.length; i++) {
            jumlah += totGpa[i];
        }
        let avgGpa = jumlah / totGpa.length
        return avgGpa;
    },
    greeting() { 
        let intro = `Namaku adalah ${this.nama} dan umurku ${this.umur} serta alamatku di ${this.alamat}. GPA rata-rataku adalah ${this.ratarataGpa()}.`;
        return intro;
    }
}; 

function latObject() {
    // console.log(`${ktp.greeting()}`);
    console.log(ktp.greeting());
    document.write(ktp.greeting());
};


// Logika lainnya untuk ratarataGPA
// nama, umur, isMarried, alamat, gpa disebut dengan properties
// greeting disebut dengan method

// for (let nilai in ktp.gpa) {
//     nilai = (ktp.gpa[0] + ktp.gpa[1] + ktp.gpa[2]) / ktp.gpa.length;
//     console.log(nilai);
// };

