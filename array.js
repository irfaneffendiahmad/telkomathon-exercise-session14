let day1 = 'senin';
let day2 = 'selasa';
let day3 = 'rabu';
let day4 = 'kamis';
let day5 = 'jumat';
let day6 = 'sabtu';
let day7 = 'minggu';

let days = ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu']; //diapit kurung siku atau square bracket

let myKtp = [17, 'messi', true, 'jl. argentina timur', undefined];

// Part 1 - Array Intro
// Pengertian array = kumpulan variabel yang berisi pasangan index dan elemen / nilai.
//                    kumpulan variabel yang berisi pasangan key dan value (key and value pair)

// Alasan pakai array:
//1. Memudahkan handle Data
// 2. Efisiensi kodingan / memori
// 3. Tidak DRY (don't repeat yourself)

// Karakteristik array:
// 1. Ada index (key) & value (nilai)
// 2. Diapit dengan kurung siku []
// 3. Bisa terdiri atas bermacam-macam tipe data
// 4. Index berurutan mulai dari 0
// 5. Array punya length (punya panjang / jumlah index)
// 6. Array memiliki method (method adalah fungsi built in)

// Cara membuat array
// Cara pertama:
let days2 = ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu'];

// Cara kedua:
let animals = [];
animals = ['kucing', 'garong', 'singa', 'macan', 'harimau', 'kelinci'];

// nested
let nestedArr = ['senin', 17, 'selasa', [40, 50, 60, ['rabu', 'kamis']], undefined, null];
// Kalo dipanggil nestedArr[3][1] --> hasilnya 50
// Kalo dipanggil nestedArr[3][3][1] --> hasilnya kamis
// Kalo diinputkan nestedArr[7] = 'bolean'; --> (8) ["senin", 17, "selasa", Array(4), undefined, null, empty, "bolean"]
// Kalo diinputkan nestedArr[6] = 'integer'; --> (8) ["senin", 17, "selasa", Array(4), undefined, null, "integer", "bolean"]
// Kalo nestedArr.push('oding'); --> (9) ["senin", 17, "selasa", Array(4), undefined, null, "integer", "bolean", "oding"]
// Kalo nestedArr.unshift('Adang'); --> (10) ["Adang", "senin", 17, "selasa", Array(4), undefined, null, "integer", "bolean", "oding"]

function learnSequence() {
    let bpName = ['jennie', 'rose', 'lisa', 'jisoo'];
    for (let sequence = 0; sequence < bpName.length; sequence++) {
        console.log(bpName[sequence]);
        document.write(bpName[sequence] + '<br/>');
    }
}

function learnSequence2() {
    let bpName = ['jennie', 'rose', 'lisa', 'jisoo'];
    for (let sequence in bpName) {
        console.log(bpName[sequence]);
        document.write(bpName[sequence] + '<br/>');
    }
}