// Struktur function
// function namaFungsi(parameter1, parameter2, paramterN) {
//     return parameter1 + parameter2 + parameterN
// }

// Cara memanggil
// namaFungsi(argument1, argument2); 
// contoh: namaFungsi(10, 20)

// let a = 1;
// let b = 2;
// let total2;
// total2 = a + b;
// console.log('total2 =>', total2);

function kalkulatortambah(bil1, bil2) { // bill1 dan bill2, namanya parameter
    let total;
    bil1 = parseInt(prompt('Masukkan angka pertama!'));
    bil2 = parseInt(prompt('Masukkan angka kedua!'));
    total = bil1 + bil2;
    alert(total); // argument --> nilai yang dimasukkan ke parameter
};

// Di dalam JS ada 2 bentuk fungsi:
// 1. Built in function
//     Contoh pada String, ada toLowerCase(), charAt(), join(), dll
// 2. User defined function
// function namaFungsi(parameter) {
    // return parameter; 
// };